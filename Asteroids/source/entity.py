"""Entity base class"""

from math import *
import random

import pygame
from pygame.locals import *

import vma
import sound

# Constant definitions:
DEFAULT_COLOR = (255, 255, 255)


#==================================================================================
# Class definition (Base):
class Entity(object):
    #------------------------------------------------------------------
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.xold = x
        self.yold = y
        self.vx = 0.0
        self.vy = 0.0
        self.ax = 0.0
        self.ay = 0.0
        self.angle = 0.0
        self.mass = 0.0
        self.hitradius = 0.0
        self.value = 0
        self.color = DEFAULT_COLOR
        self.target = None
        self.owner = None
        self.active = True
        self.mobile = True
        self.solid = True
        self.visible = True
        self.inuse = True

    #------------------------------------------------------------------
    # Semi-implicit Euler integrator for entity physics/movement:
    def move(self, now, dt):
        self.xold = self.x
        self.yold = self.y
        self.vx += self.ax * dt
        self.vy += self.ay * dt
        self.x += self.vx * dt
        self.y += self.vy * dt

    #------------------------------------------------------------------
    # Check for a collision event between two entities:
    def collide(self, other, now):
        sepx = self.x - other.x
        sepy = self.y - other.y
        seprad = self.hitradius + other.hitradius
        if (sepx*sepx + sepy*sepy < seprad*seprad):
            self.touch(other, now)
            other.touch(self, now)
            return True
        else:
            return False

    #------------------------------------------------------------------
    # Abstract passthrough (actions/AI):
    def act(self, now, dt):
        pass

    #------------------------------------------------------------------
    # Abstract passthrough (rendering):
    def draw(self, screen):
        pass

    #------------------------------------------------------------------
    # Abstract passthrough (collision handling):
    def touch(self, other, now):
        pass

    #------------------------------------------------------------------
    # Abstract passthrough (play mechanism):
    def add_score(self, amount):
        pass
