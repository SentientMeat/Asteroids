"""Vector math functions"""

from math import *

# Constant definitions:
M_ANG_PI     = radians(180.0)
M_ANG_HALFPI = radians(90.0)
M_ANG_TWOPI  = radians(360.0)

#------------------------------------------------------------------
# Returns the magnitude of the specified 2D vector:
def mag(u, v):
    return sqrt(u*u + v*v)

#------------------------------------------------------------------
# Returns the normalised form of the specified 2D vector:
def norm(u, v):
    if ((u == 0.0) and (v == 0.0)):
        return 0.0, 0.0
        
    invm = 1.0/mag(u, v)
    return invm * u, invm * v
    
#------------------------------------------------------------------
# Returns the angle value modfied to lie within [-180, 180] degs:
def wrap_angle(angle):
    if (angle > M_ANG_PI):
        angle -= M_ANG_TWOPI
    elif (angle < -M_ANG_PI):
        angle += M_ANG_TWOPI
    return angle
