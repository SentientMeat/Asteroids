"""Game states: base and derived class implementations"""

import random

import pygame
from pygame.locals import *

import sound
from entity_player import Player
from entity_rock import Rock
from entity_ufo import Ufo
from entity_bullet import Bullet
from entity_background import Background

# Constant definitions:
TITLE = "ROCKS  IN  SPAAAACE!"
FPS = 240


#------------------------------------------------------------------
# Prepares global module contants and variables:
def init():
    global FONT_TITLE
    global FONT_INFO
    global FONT_INSTR
    global FONT_STATS
    global FONT_SCORE
    FONT_TITLE = pygame.font.SysFont('Courier', 48, True, False)
    FONT_INFO  = pygame.font.SysFont('Courier', 28, True, False)
    FONT_INSTR = pygame.font.SysFont('Courier', 18, False, True)
    FONT_STATS = pygame.font.SysFont('Courier', 14, False, False)
    FONT_SCORE = pygame.font.SysFont('Courier', 20, True, False)

    global state
    state = GameMenu()


###########################################################################################################
# Class definition (Base). NOTE: There should only be one of these active:
class Game(object):
    game_dt = 1/FPS
    scrnw = 0
    scrnh = 0
    sound_fx = True
    color_backgd = (0, 0, 15)
    color_title = (255, 240, 200)
    color_instr = (180, 220, 255)
    color_action = (180, 255, 180)
    color_info = (180, 180, 180)
    color_shield = (80, 120, 200)
    color_fps = (100, 100, 100)
    color_paused = (255, 220, 200)
    color_end = (255, 150, 150)
    color_hiscore = (255, 225, 225)
    color_hiscore_name = (225, 255, 255)

    key_rotleft = K_a
    key_rotright = K_d
    key_thrust = K_w
    key_fire = K_UP
    key_shield = K_LEFT
    key_hyperspace = K_RIGHT

    _key_start = [K_RETURN, K_KP_ENTER]
    _key_quit = K_ESCAPE
    _key_toggle_pause = K_q
    _key_toggle_sound = K_e
    _key_toggle_fps = K_z
    
    _screen = None
    _entities = []
    _bullets = []
    _rocks = []
    _nrocks = 0
    _last_score = 0
    _info_actions = (("Press <Enter> to Play"), ("Press  <Esc>  to Quit"))

    #------------------------------------------------------------------
    def __init__(self):
        Game._screen = pygame.display.get_surface()
        Game.scrnw, Game.scrnh = Game._screen.get_size()

        Game._entities.clear()
        Game._bullets.clear()
        Game._rocks.clear()
        Game._nrocks = 0

    #------------------------------------------------------------------
    # Add an entity to the active list(s):
    @staticmethod
    def activate_entity(entity):
        Game._entities.append(entity)
        if (entity.__class__ == Rock):
            Game._rocks.append(entity)
            Game._nrocks += 1
        elif (entity.__class__ == Bullet):
            Game._bullets.append(entity)

    #------------------------------------------------------------------
    # Provide read-only access to the list of rocks:
    @staticmethod
    def rocks():
        return Game._rocks

    #------------------------------------------------------------------
    # Handle game events such as user input and sounds:
    def handle_events(self):
        for event in pygame.event.get():
            if (event.type == pygame.QUIT):
                return False
            elif (event.type == KEYDOWN):
                if not self.change_state(event):
                    return False
            elif (event.type >= sound.EVT_SNDMIN) and (event.type <= sound.EVT_SNDMAX):
                sound.sound_off(event.type)
        
        keystate = pygame.key.get_pressed()
        self.command(keystate)
        return True

    #------------------------------------------------------------------
    # Abstract placeholder:
    def change_state(self, event):
        pass
    
    #------------------------------------------------------------------
    # Abstract placeholder:
    def command(self, keystate):
        pass

    #------------------------------------------------------------------
    # Remove an entity from the active list(s) and delete it:
    def _delete_entity(self, entity):
        Game._entities.remove(entity)
        if (entity.__class__ == Rock):
            Game._rocks.remove(entity)
            Game._nrocks -= 1
        elif (entity.__class__ == Bullet):
            Game._bullets.remove(entity)
        elif (entity.__class__ == Ufo):
            self._reset_ufo()

        del entity

    #------------------------------------------------------------------
    # Display instructions for proceeding with the game:
    def _display_actions(self):
        y = 600
        for actstr in Game._info_actions:
            text = FONT_INSTR.render(actstr, False, Game.color_action)
            Game._screen.blit(text, [290, y])
            y += 20

    #------------------------------------------------------------------
    # Abstract placeholder:
    def _reset_ufo(self):
        pass


###########################################################################################################
# Class definition: Displays the game menu/start screen.
class GameMenu(Game):
    _info_bindings = (("    A - Rotate Left"),  ("    D - Rotate Right"), ("    W - Thrust"),
                      ("   UP - Fire"),         (" LEFT - Shield"),       ("RIGHT - Hyperspace"), (""),
                      ("    Q - Pause/Resume"), ("    E - Sound On/Off"))

    #------------------------------------------------------------------
    def __init__(self):
        Game.__init__(self)

    #------------------------------------------------------------------
    # Handle a commanded state change:
    def change_state(self, event):
        if (event.key == Game._key_quit):
            return False
        elif (event.key in Game._key_start):
            global state
            state = GameRun()

        return True

    #------------------------------------------------------------------
    # Handle player keypresses:
    def command(self, keystate):
        pass

    #------------------------------------------------------------------
    # Perform a game frame update:
    def update(self, clock):
        pass

    #------------------------------------------------------------------
    # Render the game state:
    def render(self):
        Game._screen.fill(Game.color_backgd)
        self._display_menu_info()
        pygame.display.flip()

    #------------------------------------------------------------------
    # Display the menu / instruction screen:
    def _display_menu_info(self):
        # Title:
        text = FONT_TITLE.render(TITLE, False, Game.color_title)
        Game._screen.blit(text, [(0.5*Game.scrnw) - (0.5*text.get_rect().width), 0.2*Game.scrnh])

        # Key bindings:
        y = 350
        for keystr in GameMenu._info_bindings:
            text = FONT_INSTR.render(keystr, False, Game.color_instr)
            Game._screen.blit(text, [300, y])
            y += 20

        # Actions:
        self._display_actions()


###########################################################################################################
# Class definition: Displays the 'game over' screen.
class GameOver(Game):  
    _ISCORE = 0
    _INAME = 1

    _file_hiscores = 'hiscores.dat'
    _num_scores = 10
    _name_len = 3
    _gameover_display_time = 2.0

    #------------------------------------------------------------------
    def __init__(self):
        Game.__init__(self)
        self._time = 0.0
        self._gameover_time = self._time + GameOver._gameover_display_time
        self._hiscore_data = self._get_hiscores()
        self._get_name = self._check_score_vs_hiscores(Game._last_score)
        self._name_entered = False
        self._sorted_hiscores = False
        self._name = ""
        self._name_entry = ['_', '_', '_']
        self._name_index = 0

    #------------------------------------------------------------------
    # Handle a commanded state change and (potential) player name entry:
    def change_state(self, event):
        if not self._try_get_player_name(event):
            if (event.key == Game._key_quit):
                return False
            elif (event.key in Game._key_start):
                global state
                state = GameRun()

        return True

    #------------------------------------------------------------------
    # Handle player keypresses:
    def command(self, keystate):
        pass

    #------------------------------------------------------------------
    # Perform a game frame update:
    def update(self, clock):
        self._time += Game.game_dt
        if self._get_name:
            if (self._name_entered and (not self._sorted_hiscores)):
                self._sorted_hiscores = True
                self._insert_into_hiscores((Game._last_score, self._name))
                self._save_hiscores()

    #------------------------------------------------------------------
    # Render the game state:
    def render(self):
        Game._screen.fill(Game.color_backgd)
        
        # Initially show the Game Over message, then the high-score data:
        if (self._time < self._gameover_time):
            self._display_end_info()
        else:
            self._display_hiscore_table()
            if (self._get_name and (not self._name_entered)):
                self._display_get_name()
            else:
                self._display_actions()

        pygame.display.flip()

    #------------------------------------------------------------------
    # Attempt to get the player's name:
    def _try_get_player_name(self, event):
        if (self._get_name and (not self._name_entered)):
            if ((event.key < pygame.K_a) or (event.key > pygame.K_z)):
                return True

            self._name_entry[self._name_index] = chr(event.key-32)      # force to CAPS
            self._name_index += 1
            if (self._name_index == GameOver._name_len):
                self._name_entered = True
                self._name = ''.join(self._name_entry)
            return True
        else:
            return False

    #------------------------------------------------------------------
    # Display the ending message:
    def _display_end_info(self):
        info = ' '.join("GAME OVER")
        text = FONT_INFO.render(info, False, Game.color_end)
        Game._screen.blit(text, [(0.5*Game.scrnw) - (0.5*text.get_rect().width), 0.4*Game.scrnh])

        info = "Your score was " + str(Game._last_score)
        text = FONT_INFO.render(info, False, Game.color_end)
        Game._screen.blit(text, [(0.5*Game.scrnw) - (0.5*text.get_rect().width), 0.55*Game.scrnh])

    #------------------------------------------------------------------
    # Display the high-score table:
    def _display_hiscore_table(self):
        text_header = FONT_INFO.render("HIGH SCORING PILOTS", False, Game.color_title)
        Game._screen.blit(text_header, [(0.5*Game.scrnw) - (0.5*text_header.get_rect().width), 100])

        y = 175
        for entry in self._hiscore_data:
            text_score = FONT_SCORE.render(str(entry[GameOver._ISCORE]), False, Game.color_hiscore)
            score_rect = text_score.get_rect()
            score_rect.top = y
            score_rect.right = 380
            text_name = FONT_SCORE.render(entry[GameOver._INAME], False, Game.color_hiscore_name)
            Game._screen.blit(text_score, score_rect)
            Game._screen.blit(text_name, [430, y])
            y += 30

    #------------------------------------------------------------------
    # Attempt to display a message requesting the player's name:
    def _display_get_name(self):
        info = "Enter your name: " + ' '.join(self._name_entry)
        text = FONT_INSTR.render(info, False, Game.color_action)
        Game._screen.blit(text, [(0.5*Game.scrnw) - (0.5*text.get_rect().width), 0.75*Game.scrnh])

    #------------------------------------------------------------------
    # Return True if the specified score lies within the high-score data's range:
    def _check_score_vs_hiscores(self, score):
        entrant = False
        for hiscore in self._hiscore_data:
            if (score > hiscore[GameOver._ISCORE]):
                entrant = True
                break

        return entrant

    #------------------------------------------------------------------
    # Retrieve the data from the high-scores file:
    def _get_hiscores(self):
        hiscore_data = []
        try:
            with open(GameOver._file_hiscores, 'r+') as fscores:
                for n, line in enumerate(fscores, 1):
                    entry = line.split()
                    if not entry[GameOver._ISCORE].isnumeric():
                        entry[GameOver._ISCORE] = 0
                    hiscore_data.append(((int)(entry[GameOver._ISCORE]), entry[GameOver._INAME][:GameOver._name_len]))
                    if (n == GameOver._num_scores):
                        break
        except:
            print("ERROR opening file " + GameOver._file_hiscores)

        return hiscore_data

    #------------------------------------------------------------------
    # Inserts the specified score into the high-score data and truncates that list accordingly:
    def _insert_into_hiscores(self, entry):
        sorted_list = self._hiscore_data
        sorted_list.append(entry)
        sorted_list.sort(key=lambda tup: tup[GameOver._ISCORE], reverse=True)
        self._hiscore_data = sorted_list[:GameOver._num_scores]

    #------------------------------------------------------------------
    # Writes the high-score data to the highscore data file:
    def _save_hiscores(self):
        try:
            with open(GameOver._file_hiscores, 'w') as fscores:
                for entry in self._hiscore_data:
                    fscores.write(str(entry[GameOver._ISCORE]) + ' ' + entry[GameOver._INAME] + '\n')
            fscores.close()
        except:
            print("ERROR writing to file " + GameOver._file_hiscores)


###########################################################################################################
# Class definition: Runs the game itself ... Pew pew pew!
class GameRun(Game):    
    screen_margin = 10

    _UFO_SMALL = 0
    _UFO_LARGE = 1
    
    _screen_margin_ufo = 400
    _start_lives = 3
    _start_rocks = 4
    _max_start_rocks = 12
    _levels_per_extra_rock = 3
    _rock_spawn_player_margin = 150
    _ufo_spawn_delay = 16.0
    _ufo_spawn_delay_step = 2.0
    _ufo_spawn_delay_min = 4.0
    _ufo_small_pfactor = 1.2
    _ufo_small_score = 40000
    _aggro_wave_scale = 0.5
    _endlevel_wait = 2.0
    _endgame_wait = 2.5
    _info_paused = "< PAUSED >"

    #------------------------------------------------------------------
    def __init__(self):
        Game.__init__(self)
        self._time = 0.0
        self._show_fps = False
        self._fps_val = 0

        self._player = Player(Game.scrnw/2, Game.scrnh/2, GameRun._start_lives, self._time)
        Game.activate_entity(self._player)
        self._background = Background(self._time)
        Game.activate_entity(self._background)

        self._wave = 0
        self._paused = False
        self._ufo = None
        self._ufo_active = False
        self._ufo_spawntime = 0.0
        self._num_ufos_spawned = 0
        self._endlevel = False
        self._endlevel_time = 0.0
        self._endgame = False
        self._endgame_time = 0.0
        self._try_start_new_level()        

    #------------------------------------------------------------------
    # Handle a commanded state change:
    def change_state(self, event):
        if (event.key == Game._key_quit):
            global state
            state = GameMenu()
        elif (event.key == Game._key_toggle_pause):
            self._paused = not self._paused
        elif (event.key == Game._key_toggle_sound):
            Game.sound_fx = not Game.sound_fx
        elif (event.key == Game._key_toggle_fps):
            self._show_fps = not self._show_fps
        return True

    #------------------------------------------------------------------
    # Handle player keypresses:
    def command(self, keystate):
        if self._paused:
            return

        self._player.command(keystate, self._time)

    #------------------------------------------------------------------
    # Perform a game frame update (using a fixed timestep model):
    def update(self, clock):
        if self._paused:
            return
   
        self._time += Game.game_dt
        self._fps_val = clock.get_fps()

        self._try_spawn_ufo()
        self._update_entities()
        self._check_collisions()
        self._cleanup_entities()
        self._check_for_endlevel()
        self._check_for_endgame()

    #------------------------------------------------------------------
    # Render the game state:
    def render(self):
        Game._screen.fill(Game.color_backgd)
        self._draw_entities()
        self._display_game_info()
        pygame.display.flip()

    #------------------------------------------------------------------
    # Update the entity states - moving & acting:
    def _update_entities(self):
        for ent in Game._entities:
            if ent.mobile:
                ent.move(self._time, Game.game_dt)
                self._clip_entity(ent)
            if ent.active:
                ent.act(self._time, Game.game_dt)

    #------------------------------------------------------------------
    # Enact collision events between appropriate entities:
    def _check_collisions(self):
        # Player collision with UFO:
        if (self._ufo_active and self._player.solid):
            self._player.collide(self._ufo, self._time)

        # Bullet collisions:
        for b in Game._bullets:
            if not b.solid:
                continue
            if self._player.solid:
                b.collide(self._player, self._time)
            if self._ufo_active:
                b.collide(self._ufo, self._time)
            
            for r in Game._rocks:
                if not r.solid:
                    continue
                b.collide(r, self._time)

        # Asteroid collisions:
        for r in Game._rocks:
            if not r.solid:
                continue
            if self._player.solid:
                self._player.collide(r, self._time)
            if self._ufo_active:
                self._ufo.collide(r, self._time)

    #------------------------------------------------------------------
    # Remove dead entities from the active lists:
    def _cleanup_entities(self):
        for ent in Game._entities:
            if not ent.inuse:
                self._delete_entity(ent)

    #------------------------------------------------------------------
    # Check for the end-of-level transition trigger:
    def _check_for_endlevel(self):
        if ((Game._nrocks == 0) and not self._ufo_active):
            if not self._endlevel:
                self._endlevel = True
                self._endlevel_time = self._time + GameRun._endlevel_wait
            else:
                self._try_start_new_level()
        
    #------------------------------------------------------------------
    # Check for the end-of-game transition trigger:
    def _check_for_endgame(self):
        if (self._player.nlives == 0):
            if not self._endgame:
                self._endgame = True
                self._endgame_time = self._time + GameRun._endgame_wait
                self._delete_entity(self._player)
            else:
                self._try_game_over()
                
    #------------------------------------------------------------------
    # Implement the XY wrapping of the playing area:
    def _clip_entity(self, ent):
        xmargin = GameRun._screen_margin_ufo if (ent.__class__ == Ufo) else GameRun.screen_margin
        if (ent.x < -xmargin):
            ent.x = Game.scrnw + xmargin
        elif (ent.x > Game.scrnw + xmargin):
            ent.x = -xmargin

        if (ent.y < -GameRun.screen_margin):
            ent.y = Game.scrnh + GameRun.screen_margin
        elif (ent.y > Game.scrnh + GameRun.screen_margin):
            ent.y = -GameRun.screen_margin

    #------------------------------------------------------------------
    # Draw the active entities:
    def _draw_entities(self):
        for ent in Game._entities:
            if not ent.inuse:
                continue
            if not ent.visible:
                continue
            ent.draw(Game._screen)
            
    #------------------------------------------------------------------
    # Display the game state data for the player:
    def _display_game_info(self):
        self._display_player_stats()
        self._display_player_energy()
        if self._show_fps:
            self._display_fps()
        if self._paused:
            self._display_paused()

    #------------------------------------------------------------------
    # Display Player's score and remaining lives:
    def _display_player_stats(self):
        info = []
        info.append(("LIVES: " + str(self._player.nlives), 100))
        info.append(("WAVE: " + str(self._wave), 220))
        info.append(("SCORE: " + str(self._player.score), 340))
        info.append(("ENERGY: ", 540))
        for stat in info:
            text = FONT_STATS.render(stat[0], False, Game.color_info)
            Game._screen.blit(text, [stat[1], 5])

    #------------------------------------------------------------------
    # Display Player's shield energy bank bar:
    def _display_player_energy(self):
        shield_val = Rect(600, 9, self._player.energy, 8)
        pygame.draw.rect(Game._screen, Game.color_shield, shield_val, 0)
        shield_rect = Rect(600, 8, self._player.energy_max, 10)
        pygame.draw.rect(Game._screen, Game.color_info, shield_rect, 1)

    #------------------------------------------------------------------
    # Display FPS counter:
    def _display_fps(self):
        info_fps = str((int)(self._fps_val))
        text = FONT_STATS.render(info_fps, False, Game.color_fps)
        Game._screen.blit(text, [Game.scrnw - text.get_rect().width, Game.scrnh-15])

    #------------------------------------------------------------------
    # Display Paused notification:
    def _display_paused(self):
        text = FONT_INFO.render(GameRun._info_paused, False, Game.color_paused)
        Game._screen.blit(text, [0.5*(Game.scrnw - text.get_rect().width), 0.5*(Game.scrnh - text.get_rect().height)])

    #------------------------------------------------------------------
    # Handle the transition to the next level:
    def _try_start_new_level(self):
        if (self._time < self._endlevel_time):
            return
        if self._endgame:
            return

        self._endlevel = False
        self._wave += 1
        self._num_ufos_spawned = 0
        self._reset_ufo()
        self._background.reset()
        sound.sound_reset()

        nrocks = min(GameRun._max_start_rocks, (int)((self._wave-1) / GameRun._levels_per_extra_rock) + GameRun._start_rocks)
        for i in range(nrocks):
            self._spawn_rock()

    #------------------------------------------------------------------
    # Handle the transition for the end of the game:
    def _try_game_over(self):
        if (self._time < self._endgame_time):
            return
        if self._ufo_active:
            return

        sound.sound_reset()
        Game._last_score = self._player.score
        global state
        state = GameOver()

    #------------------------------------------------------------------
    # Spawn a single asteroid (a safe distance from the player):
    def _spawn_rock(self, posx=None, posy=None, size=None):
        if (posx is None):
            xleft = (int)(self._player.x) - GameRun._rock_spawn_player_margin
            xright = (int)(self._player.x) + GameRun._rock_spawn_player_margin
            if (xleft < 0):
                posx = random.randint(xright, Game.scrnw)
            elif (xright > Game.scrnw):
                posx = random.randint(0, xleft)
            else:
                if (random.randint(0, 1) == 1):
                    posx = random.randint(0, xleft)
                else:
                    posx = random.randint(xright, Game.scrnw)

        if (posy is None):
            yupper = (int)(self._player.y) - GameRun._rock_spawn_player_margin
            ylower = (int)(self._player.y) + GameRun._rock_spawn_player_margin
            if (yupper < 0):
                posy = random.randint(ylower, Game.scrnh)
            elif (ylower > Game.scrnh):
                posy = random.randint(0, yupper)
            else:
                if (random.randint(0, 1) == 1):
                    posy = random.randint(0, yupper)
                else:
                    posy = random.randint(ylower, Game.scrnw)

        rock = Rock(posx, posy)
        Game.activate_entity(rock)

    #------------------------------------------------------------------
    # Determine if we can spawn a UFO - do so if possible:
    def _try_spawn_ufo(self):
        if (self._time < self._ufo_spawntime):
            return
        if self._ufo_active:
            return
        if self._endlevel:
            return
        if self._endgame:
            return

        self._spawn_ufo()

    #------------------------------------------------------------------
    # Spawn a UFO, with size & aggression depending on game state:
    def _spawn_ufo(self):
        x = -GameRun.screen_margin if (self._player.x < 0.5*Game.scrnw) else Game.scrnw + GameRun.screen_margin
        y = random.randrange(GameRun.screen_margin, Game.scrnh - GameRun.screen_margin)
        aggro = (int)((GameRun._aggro_wave_scale * (self._wave-1)) + self._num_ufos_spawned)
        if (self._player.score > GameRun._ufo_small_score):   # classic arcade rule
            size = GameRun._UFO_SMALL
        else:
            if (self._num_ufos_spawned == 0):
                size = GameRun._UFO_LARGE
            else:
                if (random.uniform(0.0, 1.0) > GameRun._ufo_small_pfactor / (self._num_ufos_spawned+1)):
                    size = GameRun._UFO_SMALL
                else:
                    size = GameRun._UFO_LARGE
        
        self._ufo = Ufo(x, y, size, aggro, self._player, self._time)
        Game.activate_entity(self._ufo)
        self._ufo_active = True
        self._num_ufos_spawned += 1

    #------------------------------------------------------------------
    # Prepare for spawning the next UFO (either after it has been destroyed, or at the start of a level):
    def _reset_ufo(self):
        delta = max(GameRun._ufo_spawn_delay_min, GameRun._ufo_spawn_delay - (GameRun._ufo_spawn_delay_step * self._num_ufos_spawned))
        self._ufo_spawntime = self._time + delta
        self._ufo_active = False
