"""Bullet Entity implementation"""

from entity import *


#==================================================================================
# Class definition:
class Bullet(Entity):
    _default_color = (255, 220, 220)
    _lifetime = 1.3
    _size = 1

    #------------------------------------------------------------------
    def __init__(self, x, y, vx, vy, owner, now):
        Entity.__init__(self, x, y)
        self.vx = vx
        self.vy = vy
        self.owner = owner
        self.color = Bullet._default_color
        self._size = Bullet._size
        self._deathtime = now + Bullet._lifetime

    #------------------------------------------------------------------
    # Perform entity actions:
    def act(self, now, dt):
        # Kill the bullet if it's dead o'clock:
        if (now > self._deathtime):
            self._die()

    #------------------------------------------------------------------
    # Draw the entity on the specified screen:
    def draw(self, screen):
        pygame.draw.circle(screen, self.color, (round(self.x), round(self.y)), self._size, 0)

    #------------------------------------------------------------------
    # Handle a collision:
    def touch(self, other, now):
        if (self.owner is not None):
            self.owner.add_score(other.value)

        self._die()

    #------------------------------------------------------------------
    # Handle the entity's end of life:
    def _die(self):
        self.active = False
        self.mobile = False
        self.solid = False
        self.visible = False
        self.inuse = False
        self.owner.nbullets -= 1
