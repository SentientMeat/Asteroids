"""UFO:Enemy Entity implementation"""

from entity import *
from entity_bullet import Bullet
from entity_explosion import Explosion
from entity_player import Player
import game as CGame


#==================================================================================
# Class definition:
class Ufo(Entity):
    _default_color = (255, 255, 255)
    _max_size = 1
    _ufo_small_scale = 0.6
    _mass = (2.0, 4.0)
    _hitradius = (8, 15)
    _body_thickness = 1
    _value = (1000, 200)
    _max_bullets = 3
    _bullet_speed = 450.0
    _start_fire_delay = 1.0
    _start_fire_delay_scale = 0.05
    _fire_delay_mintime = (0.7, 1.1)
    _fire_delay_maxtime = (1.2, 2.0)
    _fire_delay_scale = 0.02
    _aim_spread_base = (150.0, 300.0)
    _aim_spread_min = (50.0, 150.0)
    _aim_spread_scale = 10.0
    _velx = 100.0
    _dvy_min = (40, 45)
    _dvy_max = (80, 90)
    _dvy_mintime = (0.3, 0.9)
    _dvy_maxtime = (0.9, 1.5)

    #------------------------------------------------------------------
    def __init__(self, x, y, size, aggro, target, now):
        sound.play_effect(sound.SND_UFO)

        Entity.__init__(self, x, y)
        self.vx = Ufo._velx if (self.x < 0.5*(CGame.Game.scrnw)) else -Ufo._velx
        self.vy = 0.0
        self.color = Ufo._default_color
        self.size = size
        if (self.size > Ufo._max_size):
            self.size = Ufo._max_size
        elif (self.size < 0):
            self.size = 0

        self.mass = Ufo._mass[self.size]
        self.hitradius = Ufo._hitradius[self.size]
        self.value = Ufo._value[self.size]
        self.target = target
        self.nbullets = 0
        self._max_bullets = Ufo._max_bullets
        self._bullet_speed = Ufo._bullet_speed
        self._aggro = 0 if (aggro < 0) else aggro
        self._aim_spread = max(Ufo._aim_spread_min[self.size], Ufo._aim_spread_base[self.size] - (Ufo._aim_spread_scale * self._aggro))
        self._firetime = now + Ufo._start_fire_delay - (Ufo._start_fire_delay_scale * self._aggro)
        self._dvy_time = now + random.uniform(Ufo._dvy_mintime[self.size], Ufo._dvy_maxtime[self.size])
        self._deathtime = now + ((CGame.Game.scrnw + CGame.GameRun.screen_margin) / abs(self.vx))
        self._generate()

    #------------------------------------------------------------------
    # Perform entity actions:
    def act(self, now, dt):
        if (now > self._deathtime):
            self._die(now, False)
            return

        self._try_fire(now)
        if (now > self._dvy_time):
            self.vy = random.randrange(Ufo._dvy_min[self.size], Ufo._dvy_max[self.size])
            self.vy *= 1 if (random.randint(0, 1) == 1) else -1
            self._dvy_time = now + random.uniform(Ufo._dvy_mintime[self.size], Ufo._dvy_maxtime[self.size])

    #------------------------------------------------------------------
    # Draw the entity on the specified screen:
    def draw(self, screen):
        self._move_outline()
        pygame.draw.line(screen, self.color, (self.shape[0][0], self.shape[0][1]), (self.shape[1][0], self.shape[1][1]), Ufo._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[1][0], self.shape[1][1]), (self.shape[2][0], self.shape[2][1]), Ufo._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[2][0], self.shape[2][1]), (self.shape[3][0], self.shape[3][1]), Ufo._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[3][0], self.shape[3][1]), (self.shape[4][0], self.shape[4][1]), Ufo._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[4][0], self.shape[4][1]), (self.shape[5][0], self.shape[5][1]), Ufo._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[5][0], self.shape[5][1]), (self.shape[0][0], self.shape[0][1]), Ufo._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[1][0], self.shape[1][1]), (self.shape[4][0], self.shape[4][1]), Ufo._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[0][0], self.shape[0][1]), (self.shape[6][0], self.shape[6][1]), Ufo._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[6][0], self.shape[6][1]), (self.shape[7][0], self.shape[7][1]), Ufo._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[7][0], self.shape[7][1]), (self.shape[5][0], self.shape[5][1]), Ufo._body_thickness)

        #pygame.draw.circle(screen, (50, 80, 50), (round(self.x), round(self.y)), (int)(self.hitradius), 1)   #DEBUG: Collision circle

    #------------------------------------------------------------------
    # Handle a collision:
    def touch(self, other, now):
        if ((other.__class__ == Player) and other.shield_on):
            return

        other.add_score(self.value)
        self._die(now, True)

    #------------------------------------------------------------------
    # Generate a fixed set of vertices (shape_verts) relative to our centre:
    def _generate(self):
        verts = self._get_verts()
        
        # Transform the vertex list based on our size:
        if (self.size == 0):
            for v, vert in enumerate(verts):
                verts[v][0] *= Ufo._ufo_small_scale
                verts[v][1] *= Ufo._ufo_small_scale

        # Convert the list to a tuple as the set is now fixed:
        self.shape_verts = tuple(verts)
        
        # Prepare the list that will hold them when transformed into screen coordinates:
        nverts = len(self.shape_verts)
        self.shape = [[0 for x in range(nverts)] for y in range(nverts)]

    #------------------------------------------------------------------
    # Transform the set of relative shape vertices to screen coordinates
    # given the entity's current position:
    def _move_outline(self):
        for v, vert in enumerate(self.shape_verts):
            self.shape[v][0] = self.x + vert[0]
            self.shape[v][1] = self.y + vert[1]

    #------------------------------------------------------------------
    # Make a firing attempt against the player if the conditions are right:
    def _try_fire(self, now):
        if (now < self._firetime):
            return
        if (self.nbullets == self._max_bullets):
            return

        xaim, yaim = self._aim_at_target()
        if ((xaim is None) or (yaim is None)):
            return
        
        self._fire(xaim, yaim, now)
        upper = max(self._fire_delay_mintime[self.size], (self._fire_delay_maxtime[self.size] - (Ufo._fire_delay_scale * self._aggro)))
        delta = random.uniform(self._fire_delay_mintime[self.size], upper)
        self._firetime = now + delta

    #------------------------------------------------------------------
    # Fire a bullet at the specified position:
    def _fire(self, xaim, yaim, now):
        angle = atan2(xaim - self.x, yaim - self.y)
        sa = sin(angle)
        ca = cos(angle)
        x = self.x + ((self.hitradius + 1) * sa)
        y = self.y + ((self.hitradius + 1) * ca)
        vx = self._bullet_speed * sa
        vy = self._bullet_speed * ca
        
        sound.play_effect(sound.SND_BULLET_UFO)
        bullet = Bullet(x, y, vx, vy, self, now)
        CGame.Game.activate_entity(bullet)
        self.nbullets += 1
        
    #------------------------------------------------------------------
    # Return aiming coordinates for our target (lead it approximately):
    def _aim_at_target(self):
        if ((self.target is None) or not self.target.visible):
            return None, None

        xdist = self.target.x - self.x
        ydist = self.target.y - self.y
        fly_time = vma.mag(xdist, ydist) / self._bullet_speed
        xaim = self.target.x + (self.target.vx * fly_time) + random.randrange(-self._aim_spread, self._aim_spread)
        yaim = self.target.y + (self.target.vy * fly_time) + random.randrange(-self._aim_spread, self._aim_spread)
        return xaim, yaim

    #------------------------------------------------------------------
    # Provide the set of shape vertices relative to our position.
    # (NB: Uses a list as we may transform these based on our size.)
    def _get_verts(self):
        return [[ -4.0,  -4.0], 
                [-12.0,   0.0], 
                [ -4.0,   5.0], 
                [  4.0,   5.0], 
                [ 12.0,   0.0], 
                [  4.0,  -4.0], 
                [ -3.0, -10.0], 
                [  3.0, -10.0]]

    #------------------------------------------------------------------
    # Handle the entity's end of life:
    def _die(self, now, explode=True):
        sound.stop_effect(sound.SND_UFO)
        if explode:
            explosion = Explosion(self.x, self.y, now)
            CGame.Game.activate_entity(explosion)

        self.active = False
        self.mobile = False
        self.solid = False
        self.visible = False
        self.inuse = False
