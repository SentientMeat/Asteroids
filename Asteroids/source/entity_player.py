"""Player Entity implementation"""

from entity import *
from entity_bullet import Bullet
from entity_explosion import Explosion
import game as CGame


#==================================================================================
# Class definition:
class Player(Entity):
    _STATE_SPAWNED = 1
    _STATE_RESPAWN = 2

    _ynose = 12.0
    _ytail = 4.0
    _xwing = 10.0
    _ywing = 12.0
    _theta = radians(45.0)
    _ytail_plume = 3.0 * _ytail
    _xwing_plume = 0.6 * _xwing
    _ywing_plume = 0.6 * _ywing
    _aplume = radians(15.0)
    _plume_fac_incr = 0.04
    _plume_fac_max = 1.8

    _extra_life = 10000
    _body_color = (255, 255, 255)
    _plume_color = (160, 80, 80)
    _shield_color = (80, 120, 200)
    _shield_radius = _ynose + 3.0
    _body_thickness = 1
    _plume_thickness = 2
    _shield_thickness = 1
    _thrust = 135
    _max_speed = 450
    _space_friction = 0.003
    _rot_speed_base = 210.0
    _max_bullets = 4
    _bullet_speed = 450.0
    _bullet_vfac = 0.5
    _start_fire_delay = 0.3
    _fire_delay = 0.15
    _energy_max = 120
    _energy_charge = 1.0
    _energy_drain = 0.3
    _energy_charge_delay = 1.0
    _hyperspace_cooldown = 3.0
    _safehyper_prob = 1.0
    _safehyper_prob_decr = 0.02
    _spawn_delay = 1.5
    _respawn_rock_margin = 100
    _respawn_max_time = 5.0

    #------------------------------------------------------------------
    def __init__(self, x, y, nlives, now):
        Entity.__init__(self, x, y)
        self.nlives = nlives if (nlives > 0) else 1
        self.score = 0
        self.hitradius = Player._ynose
        self.color = Player._body_color
        self.nbullets = 0
        self.shield_on = False
        self.energy = Player._energy_max
        self.energy_max = Player._energy_max

        self._score_prev = 0
        self._plume_color = Player._plume_color
        self._shield_color = Player._shield_color
        self._energy_charge = Player._energy_charge
        self._energy_drain = Player._energy_drain
        self._energy_charge_time = 0.0
        self._energy_charge_delay = now + Player._energy_charge_delay
        self._nbounces = 0
        self._thrust = Player._thrust
        self._thrusting = False
        self._thrust_sound_on = False
        self._plume_fac = 1.0
        self._max_speed = Player._max_speed
        self._space_friction = Player._space_friction
        self._rot_speed = Player._rot_speed_base/CGame.FPS
        self._max_bullets = Player._max_bullets
        self._bullet_speed = Player._bullet_speed
        self._fire_time = now + Player._start_fire_delay
        self._fire_delay = Player._fire_delay
        self._hyperspace_time = 0.0
        self._hyperspace_cooldown = Player._hyperspace_cooldown
        self._safehyper_prob = Player._safehyper_prob

        self._state = Player._STATE_RESPAWN
        self._spawntime = 0.0
        self._spawn(x, y)

    #------------------------------------------------------------------
    # Handle the player's keypress commands:
    def command(self, keystate, now):
        if not self.visible:
            return

        direction = keystate[CGame.Game.key_rotleft] - keystate[CGame.Game.key_rotright]
        if (direction != 0):
            self._rotate(direction)
        
        self._accelerate(keystate[CGame.Game.key_thrust])
        self._try_shield(keystate[CGame.Game.key_shield])

        if (keystate[CGame.Game.key_fire]):
            self._try_fire(now)
        elif (keystate[CGame.Game.key_hyperspace]):
            self._try_hyperspace(now)

    #------------------------------------------------------------------
    # Perform entity actions:
    def act(self, now, dt):
        # Clamp to max speed (but still allow _thrust to affect our velocity):
        if (vma.mag(self.vx, self.vy) > self._max_speed):
            nx, ny = vma.norm(self.vx, self.vy)
            self.vx = nx * self._max_speed
            self.vy = ny * self._max_speed

        # Handle respawning:
        if (self._state == Player._STATE_RESPAWN):
            self._try_respawn(now)
            return

        # Handle energy bank recharging:
        self._try_energy_recharge(now)

    #------------------------------------------------------------------
    # Draw the entity on the specified screen:
    def draw(self, screen):
        # Draw the spacecraft body:
        bverts, tverts = self._get_verts()
        nv = len(bverts) - 1
        for v in range(nv):
            pygame.draw.line(screen, self.color, (bverts[v][0], bverts[v][1]), (bverts[v+1][0], bverts[v+1][1]), Player._body_thickness)
        pygame.draw.line(screen, self.color, (bverts[nv][0], bverts[nv][1]), (bverts[0][0], bverts[0][1]), Player._body_thickness)
        
        # Draw the spaecraft's shield if it's activated:
        if self.shield_on:
            pygame.draw.circle(screen, self._shield_color, (round(self.x), round(self.y)), (int)(Player._shield_radius), Player._shield_thickness)

        # Draw the thrust plume:
        ntv = len(tverts) - 1
        if (ntv < 1):
            return

        for v in range(ntv):
            pygame.draw.line(screen, self._plume_color, (tverts[v][0], tverts[v][1]), (tverts[v+1][0], tverts[v+1][1]), Player._plume_thickness)

    #------------------------------------------------------------------
    # Handle a collision:
    def touch(self, other, now):
        if (self.shield_on and (vma.mag(self.x - other.x, self.y - other.y) > other.hitradius)):
            if (other.__class__ == Bullet):
                return
            if (self._nbounces == 0):    # otherwise, we're being crushed => boom!
                self._nbounces += 1
                self._bounce(other)
                return

        self._die(now)

    #------------------------------------------------------------------
    # Bounces the spacecraft off the specified entitity (assumes circular shapes):
    # See: https://blogs.msdn.microsoft.com/faber/2013/01/09/elastic-collisions-of-balls/
    def _bounce(self, other):
        # Transform reference frame to collision axis:
        phi = atan2(self.y - other.y, self.x - other.x)
        delta_s = atan2(self.vy, self.vx) - phi
        delta_o = atan2(other.vy, other.vx) - phi
        
        v1 = vma.mag(self.vx, self.vy)
        v2 = vma.mag(other.vx, other.vy)
        v_1x = v1 * cos(delta_s)
        v_1y = v1 * sin(delta_s)
        v_2x = v2 * cos(delta_o)

        # Apply delta-v:
        if (abs(delta_s-delta_o) < vma.M_ANG_HALFPI ):      # help prevent capture by colliding entity
            v_1x = 2 * v_1x

        u_1x = (other.mass * v_2x) - v_1x

        # Transform reference frame back to window XY:
        sp = sin(phi)
        cp = cos(phi)
        self.vx = (u_1x * cp) - (v_1y * sp)
        self.vy = (u_1x * sp) - (v_1y * cp)

        # Extrapolate from our old position using our new velocity and move there:
        self.x = self.xold + self.vx * CGame.Game.game_dt
        self.y = self.yold + self.vy * CGame.Game.game_dt

    #------------------------------------------------------------------
    # Handle scoring and awarding an extra life:
    def add_score(self, amount):
        self._score_prev = self.score
        self.score += amount

        if ((int)(self.score/Player._extra_life) > (int)(self._score_prev/Player._extra_life)):
            self.nlives += 1

    #------------------------------------------------------------------
    # Initialise certain state properties when newly spawned:
    def _spawn(self, x=None, y=None, angle=None):
        self.x = x if (x is not None) else self.x
        self.y = y if (y is not None) else self.y
        self.angle = angle if (angle is not None) else -vma.M_ANG_PI
        self.vx = 0.0
        self.vy = 0.0
        self.ax = 0.0
        self.ay = 0.0
        self.active = True
        self.mobile = True
        self.solid = True
        self.visible = True
        self.shield_on = False
        self._state = Player._STATE_SPAWNED

    #------------------------------------------------------------------
    # Try to safely respawn when our local area is free from rocks.
    # If it's still not safe after a while, spawn anyway:
    def _try_respawn(self, now):
        if (now < self._spawntime):
            return

        x = 0.5 * CGame.Game.scrnw
        y = 0.5 * CGame.Game.scrnh
        safex_min = x - Player._respawn_rock_margin
        safex_max = x + Player._respawn_rock_margin
        safey_min = y - Player._respawn_rock_margin
        safey_max = y + Player._respawn_rock_margin

        safe = True
        for r in CGame.Game.rocks():
            if ((r.x > safex_min) and (r.x < safex_max) and (r.y > safey_min) and (r.y < safey_max)):
                safe = False
                break
        
        if (safe or (now > self._spawntime + Player._respawn_max_time)):
            self._spawn(x, y)
            self.energy = self.energy_max

    #------------------------------------------------------------------
    # Rotate the spacecraft by the specified amount:
    def _rotate(self, delta):
        self.angle += radians(self._rot_speed * delta)
        self.angle = vma.wrap_angle(self.angle)

    #------------------------------------------------------------------
    # Accelerate the spacecraft by the specified amount:
    def _accelerate(self, acc):
        self._thrusting = acc
        if acc:
            self.ax = self._thrust * sin(self.angle)
            self.ay = self._thrust * cos(self.angle)
            if not self._thrust_sound_on:
                self._thrust_sound_on = True
                sound.play_effect(sound.SND_THRUST)
        else:
            self.ax = 0.0
            self.ay = 0.0
            self.vx -= self.vx * self._space_friction
            self.vy -= self.vy * self._space_friction
            sound.stop_effect(sound.SND_THRUST)
            self._thrust_sound_on = False

    #------------------------------------------------------------------
    # Make a firing attempt. (NB: Can't fire through the shield.):
    def _try_fire(self, now):
        if self.shield_on:
            return
        if (self.nbullets == self._max_bullets):
            return
        if (now < self._fire_time):
            return
        
        self._fire(now)
        self._fire_time = now + self._fire_delay
        
    #------------------------------------------------------------------
    # Fire a bullet:
    def _fire(self, now):
        sa = sin(self.angle)
        ca = cos(self.angle)
        x = self.x + ((Player._ynose + 1) * sa)
        y = self.y + ((Player._ynose + 1) * ca)
        vx = (Player._bullet_vfac * self.vx) + (self._bullet_speed * sa)
        vy = (Player._bullet_vfac * self.vy) + (self._bullet_speed * ca)
        
        sound.play_effect(sound.SND_BULLET_PLY)
        bullet = Bullet(x, y, vx, vy, self, now)
        CGame.Game.activate_entity(bullet)
        self.nbullets += 1

    #------------------------------------------------------------------
    # Make a hyperspace jump attempt:
    def _try_hyperspace(self, now):
        if (now < self._hyperspace_time):
            return

        # There's always a chance of exploding...
        self._safehyper_prob -= Player._safehyper_prob_decr
        if (random.uniform(0.0, 1.0) > self._safehyper_prob):
            self._die(now)
            return
        
        # ...otherwise, make the jump:
        self._hyperspace()
        self._hyperspace_time = now + self._hyperspace_cooldown
        
    #------------------------------------------------------------------
    # Jump to a random location within the screen margins:
    def _hyperspace(self):
        x = random.randint(CGame.GameRun.screen_margin, CGame.Game.scrnw - CGame.GameRun.screen_margin)
        y = random.randint(CGame.GameRun.screen_margin, CGame.Game.scrnh - CGame.GameRun.screen_margin)
        self._spawn(x, y, self.angle)

    #------------------------------------------------------------------
    # Try to activate the spacecraft's shield:
    def _try_shield(self, activate):
        if (self.energy <= 0):
            self.shield_on = False
            return
        
        self._nbounces = 0
        self.shield_on = activate
        if self.shield_on:
            self.energy -= self._energy_drain
    
    #------------------------------------------------------------------
    # Try to recharge the shield's energy bank:
    def _try_energy_recharge(self, now):
        if (self.energy >= self.energy_max):
            return
        if self.shield_on:
            self._energy_charge_time = now + self._energy_charge_delay
            return
        if (now < self._energy_charge_time):
            return

        self.energy += self._energy_charge
        self._energy_charge_time = now + self._energy_charge_delay

    #------------------------------------------------------------------
    # Generates the body and _thrust plume vertices for the player's
    # spacecraft in screen coordinates:
    def _get_verts(self):
        # Pre-calculate (expensive) expressions that are used more than once:
        awingr = self.angle + vma.M_ANG_PI - Player._theta
        awingl = self.angle + vma.M_ANG_PI + Player._theta
        sa = sin(self.angle)
        ca = cos(self.angle)
        sawr = sin(awingr)
        cawr = cos(awingr)
        sawl = sin(awingl)
        cawl = cos(awingl)

        # Body verts:
        bverts = []
        bverts.append([self.x + (Player._ynose * sa), self.y + (Player._ynose * ca)])
        bverts.append([self.x + (Player._xwing * sawr), self.y + (Player._ywing * cawr)])
        bverts.append([self.x - (Player._ytail * sa), self.y - (Player._ytail * ca)])
        bverts.append([self.x + (Player._xwing * sawl), self.y + (Player._ywing * cawl)])

        # Thrust plume verts:
        tverts = []
        if self._thrusting:
            awingl -= Player._aplume
            awingr += Player._aplume
            tverts.append([self.x + (Player._xwing_plume * sawr), self.y + (Player._ywing_plume * cos(awingr))])
            tverts.append([self.x - (self._plume_fac * Player._ytail_plume * sa), self.y - (self._plume_fac * Player._ytail_plume * ca)])
            tverts.append([self.x + (Player._xwing_plume * sawl), self.y + (Player._ywing_plume * cawl)])
            self._plume_fac += Player._plume_fac_incr
            if (self._plume_fac > Player._plume_fac_max):
                self._plume_fac = 1.0

        return bverts, tverts
    
    #------------------------------------------------------------------
    # Handle the entity's death and respawning:
    def _die(self, now):
        if self._thrust_sound_on:
            sound.stop_effect(sound.SND_THRUST)
            self._thrust_sound_on = False

        explosion = Explosion(self.x, self.y, now)
        CGame.Game.activate_entity(explosion)

        self.nlives -= 1
        self.mobile = False
        self.solid = False
        self.visible = False
        self._state = Player._STATE_RESPAWN
        self._spawntime = now + Player._spawn_delay
