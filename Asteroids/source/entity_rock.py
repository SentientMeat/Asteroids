"""Rock Entity implementation"""

from entity import *
from entity_explosion import Explosion
from entity_player import Player
import game as CGame


#==================================================================================
# Class definition:
class Rock(Entity):
    _default_color = (255, 230, 220)
    _max_size = 2
    _body_thickness = 2
    _speed = (150, 110, 50)
    _min_speed = (90, 50, 30)
    _mass = (2.0, 3.0, 4.0)
    _value = (100, 50, 20)

    _nverts = 10
    _rad_min = 20
    _rad_max = 40
    _rad_diff_min = 10
    _ang_rnd = radians(10.0)
    _max_rad_tries = 10
    _size_scale = (0.25, 0.65, 1.0)
    _hitradius_fac = 1.1

    #------------------------------------------------------------------
    def __init__(self, x, y, vx=None, vy=None, size=None, color=None):
        Entity.__init__(self, x, y)
        self.size = size if (size is not None) else Rock._max_size
        if (self.size > Rock._max_size):
            self.size = Rock._max_size
        elif (self.size < 0):
            self.size = 0
        
        self.color = color if (color is not None) else Rock._default_color
        self.active = False
        self._set_state(vx, vy)
        self._generate()

    #------------------------------------------------------------------
    # Draw the entity on the specified screen:
    def draw(self, screen):
        self._move_outline()
        nv = len(self.shape) - 1
        for v in range(nv):
            pygame.draw.line(screen, self.color, (self.shape[v][0], self.shape[v][1]), (self.shape[v+1][0], self.shape[v+1][1]), Rock._body_thickness)
        pygame.draw.line(screen, self.color, (self.shape[nv][0], self.shape[nv][1]), (self.shape[0][0], self.shape[0][1]), Rock._body_thickness)
        
        #pygame.draw.circle(screen, (50, 80, 50), (round(self.x), round(self.y)), self.hitradius, 1)   #DEBUG: Collision circle

    #------------------------------------------------------------------
    # Handle a collision:
    def touch(self, other, now):
        if ((other.__class__ == Player) and other.shield_on):
            return
        
        other.add_score(self.value)
        self._fragment(now)
    
    #------------------------------------------------------------------
    # Generate a fixed set of vertices (shape_verts) relative to our centre:
    def _generate(self):
        verts, radii = self._get_verts()
        
        # Transform the vertex list based on our size:
        rad_sum = 0
        for v, vert in enumerate(verts):
            verts[v][0] *= Rock._size_scale[self.size]
            verts[v][1] *= Rock._size_scale[self.size]
            rad_sum += Rock._size_scale[self.size] * radii[v]

        self.hitradius = (int)(Rock._hitradius_fac * rad_sum/v)

        # Convert the list to a tuple as the set is now fixed:
        self.shape_verts = tuple(verts)
        
        # Prepare the list that will hold them when transformed into screen coordinates:
        npoints = len(self.shape_verts)
        self.shape = [[0.0 for x in range(npoints)] for y in range(npoints)]

    #------------------------------------------------------------------
    # Transform the set of relative shape vertices to screen coordinates
    # given the entity's current position:
    def _move_outline(self):
        for v, vert in enumerate(self.shape_verts):
            self.shape[v][0] = self.x + vert[0]
            self.shape[v][1] = self.y + vert[1]
            
    #------------------------------------------------------------------
    # Break into two smaller pieces, or vanish if we're the smallest size.
    def _fragment(self, now):
        explosion = Explosion(self.x, self.y, now)
        CGame.Game.activate_entity(explosion)

        self.size -= 1
        if (self.size < 0):
            self._die()
            return

        self._set_state()                                   # this rock
        self._generate()

        rock = Rock(self.x, self.y, None, None, self.size)  # second (new) rock
        CGame.Game.activate_entity(rock)

    #------------------------------------------------------------------
    # Set certain properties for a new rock state:
    def _set_state(self, vx=None, vy=None):
        self.mass = Rock._mass[self.size]
        self.value = Rock._value[self.size]
        self.hitradius = 0

        if (vx is None):
            self.vx = random.randint(Rock._min_speed[self.size], self._speed[self.size])
            if (random.randint(0, 1) == 1):
                self.vx *= -1
        else:
            self.vx = vx

        if (vy is None):
            self.vy = random.randint(Rock._min_speed[self.size], self._speed[self.size])
            if (random.randint(0, 1) == 1):
                self.vy *= -1
        else:
            self.vy = vy

    #------------------------------------------------------------------
    # Provide the set of shape vertices relative to our position.
    # This is done by procedural generatation of a rock-like shape:
    def _get_verts(self):
        verts = []
        radii = []
        rad = Rock._rad_max
        rad_prev = 0
        theta = 0.0
        delta = vma.M_ANG_TWOPI / Rock._nverts

        for v in range(Rock._nverts):
            retry = True
            cond_dir = random.randint(0, 1)
            ntries = 0
            while (retry and (ntries < Rock._max_rad_tries)):
                rad = random.randint(Rock._rad_min, Rock._rad_max)
                diff = abs(rad - rad_prev)
                retry = (diff < Rock._rad_diff_min) if (cond_dir == 0) else (diff >= Rock._rad_diff_min)
                ntries += 1
            x = rad * cos(theta)
            y = rad * sin(theta)
            verts.append([x, y])
            radii.append(rad)
            theta += delta + random.uniform(-Rock._ang_rnd, Rock._ang_rnd)
            rad_prev = rad

        return verts, radii
    
    #------------------------------------------------------------------
    # Handle the entity's end of life:
    def _die(self):
        self.mobile = False
        self.solid = False
        self.visible = False
        self.inuse = False
