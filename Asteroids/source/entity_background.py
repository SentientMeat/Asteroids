"""Boackground Sound Effects Entity implementation"""

from entity import *


#==================================================================================
# Class definition:
class Background(Entity):
    _rate_min = 0.2
    _rate_max = 1.6
    _rate_change_delay = 2.0
    _rate_change_step = -0.05

    #------------------------------------------------------------------
    def __init__(self, now):
        Entity.__init__(self, None, None)
        self.mobile = False
        self.solid = False
        self.visible = False
        self._delay = Background._rate_max
        self._play_time = now
        self._rate_change_time = now + Background._rate_change_delay

    #------------------------------------------------------------------
    # Perform entity actions:
    def act(self, now, dt):
        if (now < self._play_time):
            return

        # Gradually increase the tempo of the background theme:
        if (now > self._rate_change_time):
            self._rate_change_time = now + Background._rate_change_delay
            self._delay += Background._rate_change_step
            if (self._delay < Background._rate_min):
                self._delay = Background._rate_min

        sound.play_effect(sound.SND_BACKGROUND)
        self._play_time = now + self._delay

    #------------------------------------------------------------------
    # Reset timer:
    def reset(self):
        self._delay = Background._rate_max
        self._play_time += self._delay
        self._rate_change_time += Background._rate_change_delay

    #------------------------------------------------------------------
    # Handle the entity's end of life:
    def _die(self):
        self.active = False
        self.inuse = False
