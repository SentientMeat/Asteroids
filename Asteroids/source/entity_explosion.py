"""Explosion Entity implementation"""

from entity import *
from debris import Debris


#==================================================================================
# Class definition:
class Explosion(Entity):
    _default_color = ((255, 150, 130), (255, 255, 230))
    _bang_bits = 100
    _life_min = 0.3
    _life_rnd = 0.4
    _fade_scale = 0.4
    _rad_rnd = 10.0
    _speed_min = 30
    _speed_max = 100
    _size = 1

    #------------------------------------------------------------------
    def __init__(self, x, y, now):
        sound.play_effect(sound.SND_EXPLOSION)

        Entity.__init__(self, x, y)
        self._debris = []
        self._nbits = Explosion._bang_bits
        self.mobile = False
        self.solid = False

        angstep = vma.M_ANG_TWOPI / self._nbits
        ang = 0.0
        for i in range(self._nbits):
            r = random.uniform(0, Explosion._rad_rnd)
            ca = cos(ang)
            sa = sin(ang)
            px = x + (r * ca)
            py = y + (r * sa)
            vx = random.randint(Explosion._speed_min, Explosion._speed_max) * ca
            vy = random.randint(Explosion._speed_min, Explosion._speed_max) * sa
            lifetime = Explosion._life_min + random.uniform(0, Explosion._life_rnd)
            fadetime = Explosion._fade_scale * lifetime
            bit = Debris(px, py, vx, vy, now + lifetime, now + fadetime, Explosion._size, Explosion._default_color[Explosion._size])
            self._debris.append(bit)
            ang += angstep

    #------------------------------------------------------------------
    # Perform entity actions:
    def act(self, now, dt):
        # Move our active bits of debris:
        for bit in self._debris:
            bit.x += bit.vx * dt
            bit.y += bit.vy * dt
            if (now > bit.lifetime):
                self._debris.remove(bit)
                del bit
                self._nbits -= 1
            elif (now > bit.fadetime):
                bit.size = 0
                bit.color = Explosion._default_color[bit.size]

        # If we have no more bits left, it's time to die:
        if (self._nbits == 0):
            self._die()

    #------------------------------------------------------------------
    # Draw the entity on the specified screen:
    def draw(self, screen):
        for bit in self._debris:
            pygame.draw.circle(screen, bit.color, ((int)(bit.x), (int)(bit.y)), bit.size, 0)

    #------------------------------------------------------------------
    # Handle the entity's end of life:
    def _die(self):
        self._debris.clear()
        self.active = False
        self.visible = False
        self.inuse = False
