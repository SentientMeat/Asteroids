"""Asteroids tribute, using my 2D arcade game engine v1.0"""

import sys
import os
import traceback

import pygame
from pygame.locals import *

import sound
import game as CGame

# Constant definitions:
WINDOW_SIZE = 800, 800
WINSTYLE = 0
BIT_DEPTH = 32


#==================================================================================
# The one and only Main function:
def main():
    # Initialise the application:
    pygame.init()
    pygame.display.init()
    pygame.display.set_mode(WINDOW_SIZE, WINSTYLE, BIT_DEPTH)
    pygame.display.set_caption(CGame.TITLE)
    pygame.mouse.set_visible(False)
    
    if not sound.sound_init():
        pygame.quit()
    
    clock = pygame.time.Clock()
    CGame.init()

    # Run the main game loop:
    while CGame.state.handle_events():
        CGame.state.update(clock)
        CGame.state.render()
        clock.tick(CGame.FPS)

    # So long, and thanks for all the rocks.
    sound.sound_quit()
    pygame.quit()

#==================================================================================
# Application entry point:
if (sys.platform in ["win32", "win64"]):
    os.environ["SDL_VIDEO_CENTERED"] = "1"

if (__name__ == '__main__'):
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
