"""Debris data structure implementation"""

#==================================================================================
# Class definition:
class Debris(object):

    #------------------------------------------------------------------
    def __init__(self, x, y, vx, vy, lifetime, fadetime, size, color):
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
        self.lifetime = lifetime
        self.fadetime = fadetime
        self.size = size
        self.color = color
