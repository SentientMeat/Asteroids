"""MIDI Sound Effects module"""
# See: https://soundprogramming.net/file-formats/general-midi-instrument-list/
# Yeah, they sound a bit odd used this way :) Keeps assets internally generated for now.
# => Would use sound files for proper effects.

import pygame.midi

from game import Game

# Range of sound events:
EVT_SNDMIN = pygame.locals.USEREVENT    # NB: This only allows us to have 8 simultaneous sound events
EVT_SNDMAX = pygame.locals.NUMEVENTS

# Sound effect types:
SND_BULLET_PLY = 0
SND_BULLET_UFO = 1
SND_EXPLOSION  = 2
SND_BACKGROUND = 3
SND_THRUST     = 4
SND_UFO        = 5

# Sound effect channels:
_CHL_BULLET_PLY = 0
_CHL_BULLET_UFO = 1
_CHL_EXPLOSION  = 2
_CHL_BACKGROUND = 3
_CHL_THRUST     = 4
_CHL_UFO        = 5

# Sound effect instruments:
_INS_BULLET_PLY = 118
_INS_BULLET_UFO = 118
_INS_EXPLOSION  = 127
_INS_BACKGROUND = 18    # also: 18 44 45 55
_INS_THRUST     = 58
_INS_UFO        = 44

# Data list indices:
_ISND = 0
_IPIT = 1
_IVEL = 2
_ICHL = 3
_IDUR = 4

# Miscellaneous constants:
_LONG_TIME = 99999      # for looping FX
_MAX_VEL = 127

#------------------------------------------------------------------
# Initialisation to use the PyGame MIDI resources:
def sound_init():
    pygame.midi.init()
    global _player
    global _sound_events
    global _sound_funcs
    global _backnote

    out_id = pygame.midi.get_default_output_id()
    try:
        _player = pygame.midi.Output(out_id)
    except:
        print("ERROR setting MIDI output to " + str(out_id))
        return False

    _sound_events = []
    _backnote = 0
    _init_instruments()
    _init_sound_funcs()
    return True

#------------------------------------------------------------------
# Reset the sound module (e.g. for the end of a wave):
def sound_reset():
    global _sound_events
    global _backnote

    [sound_off(event) for event in [e for e, data in _sound_events]]
    _sound_events.clear()
    _backnote = 0

#------------------------------------------------------------------
# Clean up the MIDI resources (for closing the application):
def sound_quit():
    global _player
    del _player
    pygame.midi.quit()

#------------------------------------------------------------------
# Play the specified sound effect (if FX are toggled on):
def play_effect(effect):
    if not Game.sound_fx:
        return

    play = _sound_funcs.get(effect)
    play(effect) if (play is not None) else print("ERROR: Invalid sound effect index supplied: " + str(effect))

#------------------------------------------------------------------
# Stop playing the specified sound effect:
def stop_effect(effect):
    global _sound_events
    [sound_off(event) for event in [e for e, data in _sound_events if data[_ISND] == effect]]

#------------------------------------------------------------------
# Stops the note playing on the channel indicated by the specified
# sound event, and removes that event from the active list:
def sound_off(event):
    global _sound_events
    data = [d for e, d in _sound_events if (e == event)][0]
    [_player.note_off(pitch, data[_IVEL], data[_ICHL]) for pitch in data[_IPIT]]
    _sound_events.remove((event, data))
    pygame.time.set_timer(event, 0)

#------------------------------------------------------------------
# Print the list of (active) sound events:
def print_sound_events():
    print("_sound_events = " + str(_sound_events))

#------------------------------------------------------------------
# Allocate instruments to channels for the sound effects:
def _init_instruments():
    _player.set_instrument(_INS_BULLET_PLY, _CHL_BULLET_PLY)
    _player.set_instrument(_INS_BULLET_UFO, _CHL_BULLET_UFO)
    _player.set_instrument(_INS_EXPLOSION,  _CHL_EXPLOSION)
    _player.set_instrument(_INS_BACKGROUND, _CHL_BACKGROUND)
    _player.set_instrument(_INS_THRUST,     _CHL_THRUST)
    _player.set_instrument(_INS_UFO,        _CHL_UFO)

#------------------------------------------------------------------
# Build the dictionary of sound effect types and their functions:
def _init_sound_funcs():
    global _sound_funcs
    _sound_funcs = { SND_BULLET_PLY : _play_effect_bullet_player,
                     SND_BULLET_UFO : _play_effect_bullet_ufo,
                     SND_EXPLOSION  : _play_effect_explosion,
                     SND_BACKGROUND : _play_effect_background,
                     SND_THRUST     : _start_effect_thrust,
                     SND_UFO        : _start_effect_ufo }

#------------------------------------------------------------------
# Try to add an event to the sound events list:
# Return True if event was added.
# Data = (effect, [pitch(es)], velocity, channel, duration)
def _try_add_event(data):
    event = _get_free_event()
    if (event is None):
        print("WARNING: Sound event list is full")
        return False
    
    _sound_events.append((event, data))
    pygame.time.set_timer(event, data[_IDUR])
    return True

#------------------------------------------------------------------
# Return the next free sound event index within the allowed range:
# Return None if the list is full.
def _get_free_event():
    events = [e for e, d in _sound_events]
    try:
        event = next(filter(lambda e : events.count(e) == 0, range(EVT_SNDMIN, EVT_SNDMAX))) if events != [] else EVT_SNDMIN
    except StopIteration:
        event = None

    return event

#------------------------------------------------------------------
# Player's bullet sound effect:
def _play_effect_bullet_player(effect):
    pitch = 76
    velocity = _MAX_VEL
    channel = _CHL_BULLET_PLY
    duration = 300
    if _try_add_event((effect, [pitch], velocity, channel, duration)):
        _player.note_on(pitch, velocity, channel)

#------------------------------------------------------------------
# UFO's bullet sound effect:
def _play_effect_bullet_ufo(effect):
    pitch = 87
    velocity = _MAX_VEL
    channel = _CHL_BULLET_UFO
    duration = 400
    if _try_add_event((effect, [pitch], velocity, channel, duration)):
        _player.note_on(pitch, velocity, channel)

#------------------------------------------------------------------
# Explosion sound effect:
def _play_effect_explosion(effect):
    pitch_lo = 35
    pitch_hi = 90
    velocity = _MAX_VEL
    channel = _CHL_EXPLOSION
    duration = 600
    if _try_add_event((effect, [pitch_lo, pitch_hi], velocity, channel, duration)):
        _player.note_on(pitch_lo, velocity, channel)
        _player.note_on(pitch_hi, velocity, channel)

#------------------------------------------------------------------
# Background sound effect - alternating notes:
def _play_effect_background(effect):
    global _backnote
    _backnote = 1 - _backnote
    pitch = 60 + _backnote
    velocity = 85
    channel = _CHL_BACKGROUND
    duration = 175
    if _try_add_event((effect, [pitch], velocity, channel, duration)):
        _player.note_on(pitch, velocity, channel)

#------------------------------------------------------------------
# Thrust sound effect. Note: Requires stopping via a separate call:
def _start_effect_thrust(effect):
    pitch_lo = 10
    pitch_hi = pitch_lo + 12
    velocity = 90
    channel = _CHL_THRUST
    duration = _LONG_TIME
    if _try_add_event((effect, [pitch_lo, pitch_hi], velocity, channel, duration)):
        _player.note_on(pitch_lo, velocity, channel)
        _player.note_on(pitch_hi, velocity, channel)

#------------------------------------------------------------------
# UFO sound effect. Note: Requires stopping via a separate call:
def _start_effect_ufo(effect):
    pitch_lo = 30
    pitch_hi = 110
    velocity = 80
    channel = _CHL_UFO
    duration = _LONG_TIME
    if _try_add_event((effect, [pitch_lo, pitch_hi], velocity, channel, duration)):
        _player.note_on(pitch_lo, velocity, channel)
        _player.note_on(pitch_hi, velocity, channel)
